#!/usr/bin/python

import rospy
import tf2_ros
import tf2_msgs
import geometry_msgs.msg
import math

rospy.init_node('solarsys', anonymous=True)
rate=rospy.Rate(10)
broadcaster= tf2_ros.TransformBroadcaster()

class Planets():
    def __init__(self,lg2,radius,lg1='SUN'):
        self.radius = radius
        self.trans=geometry_msgs.msg.TransformStamped()
        self.trans.header.frame_id = lg1
        self.trans.child_frame_id = lg2
        self.trans.transform.translation.x=1
        self.trans.transform.translation.y=2
        self.trans.transform.translation.z=0
        self.trans.transform.rotation.x=0
        self.trans.transform.rotation.y=0
        self.trans.transform.rotation.z=0
        self.trans.transform.rotation.w=1

    def translation(self):
        x=2*rospy.Time.now().to_sec()*math.pi/(self.radius**(3/2)*60)
        self.trans.transform.translation.x=self.radius * math.sin(x)
        self.trans.transform.translation.y=self.radius * math.cos(x)
        self.trans.transform.translation.z=0.0
        return self.trans

mercury=Planets('MERCURY',rospy.get_param('mercury'))
venus=Planets('VENUS',rospy.get_param('venus'))
earth=Planets('EARTH',rospy.get_param('earth'))
moon=Planets('MOON',rospy.get_param('moon'),'EARTH')
mars=Planets('MARS',rospy.get_param('mars'))
phobos=Planets('PHOBOS',rospy.get_param('phobos'),'MARS')
deimos=Planets('DEIMOS',rospy.get_param('deimos'),'MARS')
jupiter=Planets('JUPITER',rospy.get_param('jupiter'))
europa=Planets('EUROPA',rospy.get_param('europa'),'JUPITER')
saturn=Planets('SATURN',rospy.get_param('saturn'))
titan=Planets('TITAN',rospy.get_param('titan'),'SATURN')
uranus=Planets('URANUS',rospy.get_param('uranus'))
neptune=Planets('NEPTUNE',rospy.get_param('neptune'))
P=[mercury,venus,earth,moon,mars,phobos,deimos,jupiter,europa,saturn,titan,uranus,neptune]


while not rospy.is_shutdown():
    for c in P:
        broadcaster.sendTransform(c.translation())
    rate.sleep()

